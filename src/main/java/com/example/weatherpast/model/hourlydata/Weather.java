package com.example.weatherpast.model.hourlydata;

public class Weather {
	
	private String icon;
	private Double code;
	private String description;
	
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Double getCode() {
		return code;
	}
	public void setCode(Double code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
