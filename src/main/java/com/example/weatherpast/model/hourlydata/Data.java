package com.example.weatherpast.model.hourlydata;

/* 
 * This class defines the Data attributes of the hourly API response from the following  
 * https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/Hourly32Historical32Weather32Data/get_history_hourly_city_city_country_country
 */

public class Data {

	private Double rh;
	private Double wind_spd;
	private String timestamp_utc;
	private Double slp;
	private String pod;
	private Double dni;
	private Double elev_angle;
	private Double pres; 
	private Double h_angle;
	private Double dewpt;
	private String snow;
	private Double uv;
	private Double solar_rad;
	private Double wind_dir; 
	private Weather weather= new Weather();
	private Double ghi;
	private Double dhi;
	private String timestamp_local; 
	private Double app_temp;
	private Double azimuth;
	private String datetime;
	private Double temp;
	private String precip;
	private Double clouds;
	private Double ts;
	
	
	public Double getRh() {
		return rh;
	}
	public void setRh(Double rh) {
		this.rh = rh;
	}
	public Double getWind_spd() {
		return wind_spd;
	}
	public void setWind_spd(Double wind_spd) {
		this.wind_spd = wind_spd;
	}
	public String getTimestamp_utc() {
		return timestamp_utc;
	}
	public void setTimestamp_utc(String timestamp_utc) {
		this.timestamp_utc = timestamp_utc;
	}
	public Double getVis() {
		return vis;
	}
	public void setVis(Double vis) {
		this.vis = vis;
	}
	public Double getSlp() {
		return slp;
	}
	public void setSlp(Double slp) {
		this.slp = slp;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public Double getDni() {
		return dni;
	}
	public void setDni(Double dni) {
		this.dni = dni;
	}
	public Double getElev_angle() {
		return elev_angle;
	}
	public void setElev_angle(Double elev_angle) {
		this.elev_angle = elev_angle;
	}
	public Double getPres() {
		return pres;
	}
	public void setPres(Double pres) {
		this.pres = pres;
	}
	public Double getH_angle() {
		return h_angle;
	}
	public void setH_angle(Double h_angle) {
		this.h_angle = h_angle;
	}
	public Double getDewpt() {
		return dewpt;
	}
	public void setDewpt(Double dewpt) {
		this.dewpt = dewpt;
	}
	public String getSnow() {
		return snow;
	}
	public void setSnow(String snow) {
		if (snow == null) 
			this.snow="0";		
		else
			this.snow = snow;
	}
	public Double getUv() {
		return uv;
	}
	public void setUv(Double uv) {
		this.uv = uv;
	}
	public Double getSolar_rad() {
		return solar_rad;
	}
	public void setSolar_rad(Double solar_rad) {
		this.solar_rad = solar_rad;
	}
	public Double getWind_dir() {
		return wind_dir;
	}
	public void setWind_dir(Double wind_dir) {
		this.wind_dir = wind_dir;
	}
	public Weather getWeather() {
		return weather;
	}
	public void setWeather(Weather weather) {
		this.weather = weather;
	}
	public Double getGhi() {
		return ghi;
	}
	public void setGhi(Double ghi) {
		this.ghi = ghi;
	}
	public Double getDhi() {
		return dhi;
	}
	public void setDhi(Double dhi) {
		this.dhi = dhi;
	}
	public String getTimestamp_local() {
		return timestamp_local;
	}
	public void setTimestamp_local(String timestamp_local) {
		this.timestamp_local = timestamp_local;
	}
	public Double getApp_temp() {
		return app_temp;
	}
	public void setApp_temp(Double app_temp) {
		this.app_temp = app_temp;
	}
	public Double getAzimuth() {
		return azimuth;
	}
	public void setAzimuth(Double azimuth) {
		this.azimuth = azimuth;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public Double getTemp() {
		return temp;
	}
	public void setTemp(Double temp) {
		this.temp = temp;
	}
	public String getPrecip() {
		return precip;
	}
	public void setPrecip(String precip) {
		if(precip==null)
			this.precip="0";
		else
			this.precip = precip;
	}
	public Double getClouds() {
		return clouds;
	}
	public void setClouds(Double clouds) {
		this.clouds = clouds;
	}
	public Double getTs() {
		return ts;
	}
	public void setTs(Double ts) {
		this.ts = ts;
	}
	private Double vis;
	
	
}
