package com.example.weatherpast.model.hourlydata;

import java.time.LocalTime;

/*
 * This class is responsible to represent Hourly Snow and Precipitation amount from the hourly complete API response
 * 
 */

public class HourlyPrecipitationData {
	
	private LocalTime time;
	private String snow;
	private String precipitation;
	
	
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	
	public String getSnow() {
		return snow;
	}
	public void setSnow(String snow) {
		this.snow = snow;
	}
	public String getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(String precipitation) {
		this.precipitation = precipitation;
	} 

}
