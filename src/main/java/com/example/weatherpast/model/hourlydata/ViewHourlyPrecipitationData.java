package com.example.weatherpast.model.hourlydata;

/*
 * This class is responsible to show hourly data with a time range
 */
public class ViewHourlyPrecipitationData {

	private String time_range;
	private String snow;
	private String precipitation;	
	
	public String getTime_range() {
		return time_range;
	}
	public void setTime_range(String time_range) {
		this.time_range = time_range;
	}
		
	public String getSnow() {
		return snow;
	}
	public void setSnow(String snow) {
		this.snow = snow;
	}
	public String getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(String precipitation) {
		this.precipitation = precipitation;
	} 
	
}
