package com.example.weatherpast.model.hourlydata;

import java.util.ArrayList;

/* 
 * This class defines the attributes to consume hourly API response from the following  
 * https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/Hourly32Historical32Weather32Data/get_history_hourly_city_city_country_country
 */

public class HourlyWeatherData {

	private String timezone;
	private String state_code;
	private String country_code;
	private Double lat;
	private Double lon;
	private String city_name;
	private String station_id;
	private ArrayList<String> sources = new ArrayList<String>();
	private ArrayList<Data> data = new ArrayList<Data>();
	private String city_id;
	
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getStation_id() {
		return station_id;
	}
	public void setStation_id(String station_id) {
		this.station_id = station_id;
	}
	public ArrayList<String> getSources() {
		return sources;
	}
	public void setSources(ArrayList<String> sources) {
		this.sources = sources;
	}
	public ArrayList<Data> getData() {
		return data;
	}
	public void setData(ArrayList<Data> data) {
		this.data = data;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}

}
