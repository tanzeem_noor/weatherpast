package com.example.weatherpast.model.dailydata;

import java.util.ArrayList;

/*
 *This class defines the attributes to consume daily API response from the following  
 * https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/Daily32Historical32Weather32Data/get_history_daily_city_city_country_country
 * 
 */
public class DailyWeatherData {
	
	private String timezone;
	private String state_code;
	private String country_code;
	private double lat;
	private double lon;
	private String city_name;
	private String station_id;
	private ArrayList<Data> data = new ArrayList<Data>();
	private ArrayList<String> sources = new ArrayList<String>();
	private String city_id;
	
	
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getStation_id() {
		return station_id;
	}
	public void setStation_id(String station_id) {
		this.station_id = station_id;
	}
	
	public ArrayList<Data> getData() {
		return data;
	}
	public void setData(ArrayList<Data> data) {
		this.data = data;
	}
	
	public ArrayList<String> getSources() {
		return sources;
	}
	public void setSources(ArrayList<String> sources) {
		this.sources = sources;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}
	
	
}
