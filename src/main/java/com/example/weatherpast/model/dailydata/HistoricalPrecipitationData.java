package com.example.weatherpast.model.dailydata;

/*
 * This class contains the required attribute to call the API and transfer between MVC 
 */
public class HistoricalPrecipitationData {
		
		private String date_start;
		private String date_end;
		private String city;
		private String state;
		private String country;
		
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getDate_start() {
			return date_start;
		}
		public void setDate_start(String date_start) {
			this.date_start = date_start;
		}
		public String getDate_end() {
			return date_end;
		}
		public void setDate_end(String date_end) {
			this.date_end = date_end;
		}
}
