package com.example.weatherpast.model.dailydata;

/* 
* This class defines the Data attributes of the daily API response from the following  
* https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/Daily32Historical32Weather32Data/get_history_daily_city_city_country_country
* 
*/

public class Data {

	private double rh;
	private double max_wind_spd_ts;
	private double t_ghi;
	private double max_wind_spd;
	private double wind_gust_spd;
	private double max_temp_ts;
	private double snow_depth;
	private double clouds;
	private double max_dni;
	private double precip_gpm;
	private double wind_spd;
	private double slp;
	private double ts;
	private double max_ghi;
	private double pres;
	private double dni;
	private double dewpt;
	private double snow;
	private double t_dhi;
	private double precip;
	private double wind_dir;
	private double max_dhi;
	private double ghi;
	private double max_temp;
	private double t_dni;
	private double max_uv;
	private double dhi;
	private String datetime;
	private double temp;
	private double min_temp;
	private double max_wind_dir;
	private double min_temp_ts;
	
	
	public double getRh() {
		return rh;
	}
	public void setRh(double rh) {
		this.rh = rh;
	}
	public double getMax_wind_spd_ts() {
		return max_wind_spd_ts;
	}
	public void setMax_wind_spd_ts(double max_wind_spd_ts) {
		this.max_wind_spd_ts = max_wind_spd_ts;
	}
	public double getT_ghi() {
		return t_ghi;
	}
	public void setT_ghi(double t_ghi) {
		this.t_ghi = t_ghi;
	}
	public double getMax_wind_spd() {
		return max_wind_spd;
	}
	public void setMax_wind_spd(double max_wind_spd) {
		this.max_wind_spd = max_wind_spd;
	}
	public double getWind_gust_spd() {
		return wind_gust_spd;
	}
	public void setWind_gust_spd(double wind_gust_spd) {
		this.wind_gust_spd = wind_gust_spd;
	}
	public double getMax_temp_ts() {
		return max_temp_ts;
	}
	public void setMax_temp_ts(double max_temp_ts) {
		this.max_temp_ts = max_temp_ts;
	}
	public double getSnow_depth() {
		return snow_depth;
	}
	public void setSnow_depth(double snow_depth) {
		this.snow_depth = snow_depth;
	}
	public double getClouds() {
		return clouds;
	}
	public void setClouds(double clouds) {
		this.clouds = clouds;
	}
	public double getMax_dni() {
		return max_dni;
	}
	public void setMax_dni(double max_dni) {
		this.max_dni = max_dni;
	}
	public double getPrecip_gpm() {
		return precip_gpm;
	}
	public void setPrecip_gpm(double precip_gpm) {
		this.precip_gpm = precip_gpm;
	}
	public double getWind_spd() {
		return wind_spd;
	}
	public void setWind_spd(double wind_spd) {
		this.wind_spd = wind_spd;
	}
	public double getSlp() {
		return slp;
	}
	public void setSlp(double slp) {
		this.slp = slp;
	}
	public double getTs() {
		return ts;
	}
	public void setTs(double ts) {
		this.ts = ts;
	}
	public double getMax_ghi() {
		return max_ghi;
	}
	public void setMax_ghi(double max_ghi) {
		this.max_ghi = max_ghi;
	}
	public double getPres() {
		return pres;
	}
	public void setPres(double pres) {
		this.pres = pres;
	}
	public double getDni() {
		return dni;
	}
	public void setDni(double dni) {
		this.dni = dni;
	}
	public double getDewpt() {
		return dewpt;
	}
	public void setDewpt(double dewpt) {
		this.dewpt = dewpt;
	}
	public double getSnow() {
		return snow;
	}
	public void setSnow(double snow) {
		this.snow = snow;
	}
	public double getT_dhi() {
		return t_dhi;
	}
	public void setT_dhi(double t_dhi) {
		this.t_dhi = t_dhi;
	}
	public double getPrecip() {
		return precip;
	}
	public void setPrecip(double precip) {
		this.precip = precip;
	}
	public double getWind_dir() {
		return wind_dir;
	}
	public void setWind_dir(double wind_dir) {
		this.wind_dir = wind_dir;
	}
	public double getMax_dhi() {
		return max_dhi;
	}
	public void setMax_dhi(double max_dhi) {
		this.max_dhi = max_dhi;
	}
	public double getGhi() {
		return ghi;
	}
	public void setGhi(double ghi) {
		this.ghi = ghi;
	}
	public double getMax_temp() {
		return max_temp;
	}
	public void setMax_temp(double max_temp) {
		this.max_temp = max_temp;
	}
	public double getT_dni() {
		return t_dni;
	}
	public void setT_dni(double t_dni) {
		this.t_dni = t_dni;
	}
	public double getMax_uv() {
		return max_uv;
	}
	public void setMax_uv(double max_uv) {
		this.max_uv = max_uv;
	}
	public double getDhi() {
		return dhi;
	}
	public void setDhi(double dhi) {
		this.dhi = dhi;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public double getTemp() {
		return temp;
	}
	public void setTemp(double temp) {
		this.temp = temp;
	}
	public double getMin_temp() {
		return min_temp;
	}
	public void setMin_temp(double min_temp) {
		this.min_temp = min_temp;
	}
	public double getMax_wind_dir() {
		return max_wind_dir;
	}
	public void setMax_wind_dir(double max_wind_dir) {
		this.max_wind_dir = max_wind_dir;
	}
	public double getMin_temp_ts() {
		return min_temp_ts;
	}
	public void setMin_temp_ts(double min_temp_ts) {
		this.min_temp_ts = min_temp_ts;
	}
	
}
