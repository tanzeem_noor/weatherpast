package com.example.weatherpast.model.dailydata;

import java.time.LocalDate;

/*
* This class is responsible to represent Daily Snow and Precipitation amount from the hourly complete API response
* 
*/

public class DailyPrecipitaionData {

	private LocalDate date;
	private String snow;
	private String precipitation;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	public String getSnow() {
		return snow;
	}
	public void setSnow(String snow) {
		this.snow = snow;
	}
	public String getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(String precipitation) {
		this.precipitation = precipitation;
	} 
}
