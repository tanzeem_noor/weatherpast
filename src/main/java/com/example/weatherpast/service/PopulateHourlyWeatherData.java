package com.example.weatherpast.service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;


import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.example.weatherpast.model.hourlydata.HourlyWeatherData;
import com.example.weatherpast.model.hourlydata.ViewHourlyPrecipitationData;

public class PopulateHourlyWeatherData {
	
private static String api_key="b61180b09e1b418a93e94b6333f380cc";
	
public static ArrayList<ViewHourlyPrecipitationData> generatePastWeatherHourlyData(String specific_date,String end_date,String city, String country,String state) {		
		
		String api_call_prefix="https://api.weatherbit.io/v2.0/history/hourly?";
		String api_city_info = "city="+city+"&country="+country+"&state="+state;
		String api_call=api_call_prefix+api_city_info+"&start_date="+specific_date+"&end_date="+end_date+"&key="+api_key;
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		RestTemplate restTemplate = new RestTemplate();		
		HourlyWeatherData hourlyWeatherData = restTemplate.getForObject(api_call, HourlyWeatherData.class, 200);
		
		ArrayList<ViewHourlyPrecipitationData> view_hourly_data_list = new ArrayList<ViewHourlyPrecipitationData>();
		int view_total_hours = hourlyWeatherData.getData().size();
		
		for(int i=0;i<view_total_hours;i++) {
			
			String date_time = hourlyWeatherData.getData().get(i).getTimestamp_utc();
			String snow = hourlyWeatherData.getData().get(i).getSnow();
			String precip = hourlyWeatherData.getData().get(i).getPrecip();		
			
			ViewHourlyPrecipitationData view_hourly_data=new ViewHourlyPrecipitationData();
			
			LocalTime t1=LocalDateTime.parse(date_time).toLocalTime(); //convert localdatetime to local time
			LocalTime t2=t1.plusHours(1); //add an hour for 2nd part, e.g., 01:00 - 02:00
			
			//to show time as range 
			String time_range=t1.toString()+ " - " + t2.toString();
			
			view_hourly_data.setTime_range(time_range);
			view_hourly_data.setSnow(snow);
			view_hourly_data.setPrecipitation(precip);
			
			view_hourly_data_list.add(view_hourly_data);
		}	
		
		return view_hourly_data_list;
	}
}