package com.example.weatherpast.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;


import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.example.weatherpast.model.dailydata.DailyPrecipitaionData;
import com.example.weatherpast.model.dailydata.DailyWeatherData;

public class PopulateDailyWeatherData {
	
	private static String api_key="b61180b09e1b418a93e94b6333f380cc";	

	public static ArrayList<DailyPrecipitaionData> generatePastWeatherData(String start_date,String end_date,String city, String state, String country) {
		
		String api_call_prefix="https://api.weatherbit.io/v2.0/history/daily?";
		String api_city_info = "city="+city+"&country="+country+"&&state="+state;
		String api_call=api_call_prefix+api_city_info+"&start_date="+start_date+"&end_date="+end_date+"&key="+api_key;
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		RestTemplate restTemplate = new RestTemplate();		
		DailyWeatherData dailyWeatherData = restTemplate.getForObject(api_call, DailyWeatherData.class, 200);
						
		int total_days = dailyWeatherData.getData().size();		
		ArrayList<DailyPrecipitaionData> daily_data_list = new ArrayList<DailyPrecipitaionData>();
				
		for(int i=0;i<total_days;i++) {
			
			String date = dailyWeatherData.getData().get(i).getDatetime();			
			double snow = dailyWeatherData.getData().get(i).getSnow();
			double precip = dailyWeatherData.getData().get(i).getPrecip();
		
			//prepare daily data object with date time and precipitation
			DailyPrecipitaionData daily_data=new DailyPrecipitaionData();
			daily_data.setDate(LocalDate.parse(date));
			daily_data.setPrecipitation(Double.toString(precip));
			daily_data.setSnow(Double.toString(snow));
			
			daily_data_list.add(daily_data);

			
		}
		
		// Sort in ascending order by date
        Collections.sort(daily_data_list, new Comparator<DailyPrecipitaionData>() {
            public int compare(DailyPrecipitaionData d1, DailyPrecipitaionData d2) {
                return (d1.getDate()).compareTo(d2.getDate());
            }
        });
		
        return daily_data_list;
	}
	
}
