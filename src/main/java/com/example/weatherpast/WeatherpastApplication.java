package com.example.weatherpast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WeatherpastApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherpastApplication.class, args);
	}

}
