package com.example.weatherpast.controllers;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.weatherpast.model.dailydata.DailyPrecipitaionData;
import com.example.weatherpast.model.dailydata.HistoricalPrecipitationData;
import com.example.weatherpast.model.hourlydata.ViewHourlyPrecipitationData;
import com.example.weatherpast.service.PopulateDailyWeatherData;
import com.example.weatherpast.service.PopulateHourlyWeatherData;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class WeatherController {
	
	@RequestMapping("/")
	public String getPastWeatherData(Model model){
		
		int how_many_days_data=30;
		
		String end_date=LocalDate.now().toString(); //get current system date		
		String start_date=LocalDate.now().minusDays(how_many_days_data).toString(); //get start date
		String city="Toronto";
		String country="CA";
		String state="Ontario";
		
		model.addAttribute("start_date",start_date);
		model.addAttribute("end_date",end_date);
		model.addAttribute("city",city);
		model.addAttribute("country",country);
		model.addAttribute("state",state);		
		
		ArrayList<DailyPrecipitaionData> daily_weather_data_list = new ArrayList<DailyPrecipitaionData>();
		daily_weather_data_list=PopulateDailyWeatherData.generatePastWeatherData(start_date, end_date, city, state, country);
		
		model.addAttribute("daily_weather_data_list", daily_weather_data_list);
		
		return "weatherpast";
	}
	
	@RequestMapping(value ="/hourly-data/{city}/{state}/{country}/{date}",method = RequestMethod.GET)
	public String getPastWeatherHourlyData(@PathVariable("city") String city, @PathVariable("state") String state,@PathVariable("country") String country,@PathVariable("date") String specific_date, Model dailydatamodel){
	
		dailydatamodel.addAttribute("city",city);
		dailydatamodel.addAttribute("state",state);
		dailydatamodel.addAttribute("country",country);
		dailydatamodel.addAttribute("date",specific_date);
		
		LocalDate next_date = LocalDate.parse(specific_date).plusDays(1); //add one day to the specific date as api requires an end date
		
		ArrayList<ViewHourlyPrecipitationData> view_hourly_weather_data_list = new ArrayList<ViewHourlyPrecipitationData>();
		view_hourly_weather_data_list=PopulateHourlyWeatherData.generatePastWeatherHourlyData(specific_date, next_date.toString(), city, country, state);
		
		dailydatamodel.addAttribute("view_hourly_weather_data_list", view_hourly_weather_data_list);
		
		return "hourlydata";
	}
	
	@RequestMapping(value ="/historical-data",method = RequestMethod.POST)
	 public String getCustomHistoricalData(@ModelAttribute("historicaldata") HistoricalPrecipitationData hprecip, Model historicaldatamodel){  
        
		historicaldatamodel.addAttribute("start_date",hprecip.getDate_start());		
		historicaldatamodel.addAttribute("end_date",hprecip.getDate_end());		
		historicaldatamodel.addAttribute("city",hprecip.getCity());
		historicaldatamodel.addAttribute("state",hprecip.getState());
		historicaldatamodel.addAttribute("country",hprecip.getCountry());	
		
		//add one day with the end date selected as api end date gets data excluding date provided
		String end_date=LocalDate.parse(hprecip.getDate_end()).plusDays(1).toString();
				
        ArrayList<DailyPrecipitaionData> historical_weather_data_list = new ArrayList<DailyPrecipitaionData>();
		historical_weather_data_list=PopulateDailyWeatherData.generatePastWeatherData(hprecip.getDate_start(), end_date, hprecip.getCity(), hprecip.getState(), hprecip.getCountry());
		
		historicaldatamodel.addAttribute("historical_weather_data_list", historical_weather_data_list);

		return "customdatedailydata";
    }  

	
}