<html>
<head>
	<title>Welcome to Past Weather Data</title>

	<link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">
	
	<script src="${pageContext.request.contextPath}/webjars/jquery/1.9.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
	<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
</head>

<body>

	

	<div class="center">
	
	<div style="height:40px; width:750px; background-color:BlanchedAlmond">
	  <h2>Snow and Precipitation amount in ${city}, ${state}, ${country} </h2>
	</div>
	
		<div>
			<p></p>
			<p> Start Date (YYYY-MM-DD): ${start_date} </p>
			<p> End Date (YYYY-MM-DD): ${end_date} </p>
		</div>	
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
		<table class="table table-striped table1">
<%-- 			<caption>Snow and Precipitation History in ${city}, ${state}, ${country} between ${start_date} and ${end_date}</caption> --%>
			<thead>
				<tr>
					<th>Date (YYYY-MM-DD)</th>
					<th>Snow (mm)</th>
					<th>Precipitation (mm)</th>
					<th>Details</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach var="entry" items="${historical_weather_data_list}">
		    		<tr>
		        		<td>${entry.date}</td>     		
		        		<td>${entry.snow}</td>
	           			<td>${entry.precipitation}</td>
	           
	           			<td><a class="btn btn-info" href="${pageContext.request.contextPath}/hourly-data/${city}/${state}/${country}/${entry.date}">View Hourly Data</a></td>
		        	</tr>
				</c:forEach>
			</tbody>
		</table>
		
		
<%-- 		<script src="${pageContext.request.contextPath}/js/custom.js"></script> --%>
	</div>
</body>
</html>