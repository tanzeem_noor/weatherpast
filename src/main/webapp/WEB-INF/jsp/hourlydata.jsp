<html>
<head>
	<title>Historical Weather Data</title>
	
<link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">
	
</head>
<body>

	
	
	<div class="center">
	
	<div style="height:40px; width:650px; background-color:BlanchedAlmond" >
	  <h4>Hourly Snow and Precipitation amount in ${city}, ${state}, ${country} on ${date} </h4>
		<p></p>
	</div>
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
		<table class="table table-striped" id="table1" >
<%-- 			<caption>Hourly Snow and Precipitation Amount in ${city}, ${state}, ${country} on ${date}</caption> --%>
		<thead>
				<tr>
					<th>Time (hh:mm)</th>
					<th>Snow (mm)</th>
					<th>Precipitation (mm)</th>
				</tr>
			</thead>
			
			 <tbody>
				<c:forEach var="entry" items="${view_hourly_weather_data_list}">
		    		<tr>
		        		<td>${entry.time_range}</td>
		        		<td>${entry.snow}</td>
		        		<td>${entry.precipitation}</td>     		
		        	</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<script src="${pageContext.request.contextPath}/webjars/jquery/1.9.1/jquery.min.js"></script>
		<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<%-- 		<script src="${pageContext.request.contextPath}/js/custom.js"></script> --%>
	</div>
</body>
</html>