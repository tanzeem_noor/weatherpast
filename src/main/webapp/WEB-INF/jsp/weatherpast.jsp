<html>
<head>
	<title>Historical Weather Data</title>

	<link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">
	
	<script src="${pageContext.request.contextPath}/webjars/jquery/1.9.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
	<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
		
</head>

<body>
	

  	<script>
  	function checkforblankDate(){
  		
  		if(document.getElementById('date_start').value == ""){  			
  			alert('Please select start date');
  			return false;
  		}
  		
  		if(document.getElementById('date_end').value == ""){  			
  			alert('Please select end date');
  			return false;
  		}
  	}
  	</script>
  
  	<script>
  	 $(function () {
  		 
  		 $("#date_start").datepicker({
  	        format: 'yyyy-mm-dd',
	       	autoclose: false,
	        todayBtn: true,
	        ignoreReadonly: true,
	        startDate: '-365d',
	        endDate: '-1d'	        
  	    }).on('changeDate', function (selected) {
  	        var minDate = new Date(selected.date.valueOf());
  	        $('#date_end').datepicker('setStartDate', minDate);
  	    });
  	    
  	    $("#date_end").datepicker({
 	        format: 'yyyy-mm-dd',
 	       	startDate: '-364d',
 	        endDate: '-0d',
 	        datesDisabled: '-0d',
 	        ignoreReadonly: true,
 	        autoclose: false,
 	        todayBtn: true
 	    })
  	        .on('changeDate', function (selected) {
  	            var minDate = new Date(selected.date.valueOf());
  	            $('#date_start').datepicker('setEndDate', minDate);
  	        }); 		 
  		 
  	 });
  	</script>
<div class="center">
	<div style="height:50px; width:750px; background-color:BlanchedAlmond">
	  <h2>Snow and Precipitation amount in ${city}, ${state}, ${country} </h2>
	</div>
	
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	
	 <form action="${pageContext.request.contextPath}/historical-data" method= "post" modelAttribute="historicaldata" onsubmit="return checkforblankDate()">	 
	 	
	 	<div>
	 		<p></p>
	 		
			<p> Start Date (YYYY-MM-DD): ${start_date} &emsp;&emsp;&emsp;&nbsp; Change Start Date: <input type="text" name ="date_start" id="date_start" readonly /></p>
             
			<p> End Date (YYYY-MM-DD): ${end_date} &emsp;&emsp;&emsp;&nbsp; Change End Date: <input type="text" name="date_end" id="date_end" readonly /></p>
		</div>		 	
	 	
	 	<input type="hidden" name="city" value="${city}" id={city}/>
	 	<input type="hidden" name="state" value="${state}" id={state}/>
	 	<input type="hidden" name="country" value="${country}" id={country}/>
	 	
	 	<input type="submit" value="View Data"/>
	</form>
        
        

	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
		<table class="table table-striped" id="table1" >
			<thead>
				<tr>
					<th>Date (YYYY-MM-DD)</th>
					<th>Snow (mm)</th>
					<th>Precipitation (mm)</th>
					<th>View Details</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach var="entry" items="${daily_weather_data_list}">
		    		<tr>
		        		<td>${entry.date}</td>     		
		        		<td>${entry.snow}</td>
	           			<td>${entry.precipitation}</td>
	           
	           			<td><a class="btn btn-info" href="${pageContext.request.contextPath}/hourly-data/${city}/${state}/${country}/${entry.date}">View Hourly Data</a></td>
		        	</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>